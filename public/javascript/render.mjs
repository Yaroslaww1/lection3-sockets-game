import { emitKeyboardEvent } from "./game.mjs";
import { emitUpdateUser } from "./game.mjs";
import { highlight } from "./helper.mjs";
import { createElement } from "./helper.mjs";
import { showModal } from "./modal.mjs";

const gloabalUsername = sessionStorage.getItem("username");

const roomsListWrapperElement = document.getElementById('rooms-list-wrapper');
const roomWrapperElement = document.getElementById('room-wrapper');
const roomStartTimer = document.getElementById('room-start-timer');
const roomBackButton = document.getElementById('room-back-button');
const roomReadyButton = document.getElementById('room-ready-button');
const roomText = document.getElementById('room-text');
const roomSecondsLeft = document.getElementById('room-seconds-left');

let isGameStarted = false;
let text = '';
let currentUser = {};

const updateUser = user => {
  currentUser.isReady = !currentUser.isReady;
  emitUpdateUser(user);
}

roomReadyButton.addEventListener('click', () => {updateUser(currentUser);});

// Renders list of rooms
export const renderRoomsList = (createRoom, joinRoom, rooms) => {
  const createRoomButton = document.getElementById('create-room');
  createRoomButton.addEventListener('click', createRoom);

  roomsListWrapperElement.style.display = 'flex';
  roomWrapperElement.style.display = 'none';
  
  const allRooms = rooms.map(room => {
    const { name, users } = room;
    const roomElement = createElement({
      tagName: "div",
      className: "room-item no-select",
      attributes: { id: name }
    })
    const roomNameElement = createElement({
      tagName: "h3",
      className: "room-item-name no-select"
    });
    roomNameElement.innerHTML = `${name}`;
    const roomCounterElement = createElement({
      tagName: "h4",
      className: "room-item-counter no-select"
    })
    roomCounterElement.innerHTML = `${users.length} users connected`;
    const joinButtonElement = createElement({
      tagName: "button",
      attributes: { id: `${name}-join-button` }
    })
    joinButtonElement.innerHTML = 'JOIN'
    joinButtonElement.addEventListener('click', () => {
      joinRoom(name);
      renderRoom(room);
    });
    roomElement.append(roomCounterElement);
    roomElement.append(roomNameElement);
    roomElement.append(joinButtonElement);
    return roomElement;
  });
  const roomsContainer = document.getElementById('rooms-container');
  roomsContainer.innerHTML = "";
  roomsContainer.append(...allRooms);
}

// Get user HTML element
const getUserElement = user => {
  const { username, isReady } = user;
  const userElement = createElement({
    tagName: "div",
    className: "user",
    attributes: { id: `${username}-user-wrapper` }
  });
  // Contain status && name
  const userHeaderElement = createElement({
    tagName: "div",
    className: "user-header"
  });
  const userUsernameElement = createElement({
    tagName: "div",
    className: "user-username",
    attributes: { id: `${username}-user-username` }
  })
  userUsernameElement.innerHTML = `${username} ${username === gloabalUsername ? '(you)' : ''}`

  const userReadyElement = createElement({
    tagName: "div",
    className: `user-ready-status ${isReady === true ? 'green' : 'red'}`,
    attributes: { id: `${username}-user-ready-status` }
  });

  const userProgressWrapperElement = createElement({
    tagName: "div",
    className: "user-progress-wrapper",
    attributes: { 
      id: `${username}-user-progress-wrapper`
    }
  });
  const userProgressElement = createElement({
    tagName: "div",
    className: "user-progress",
    attributes: { 
      id: `${username}-user-progress`
    }
  });
  userProgressWrapperElement.append(userProgressElement);

  userHeaderElement.append(userReadyElement);
  userHeaderElement.append(userUsernameElement);
  
  userElement.append(userHeaderElement);
  userElement.append(userProgressWrapperElement);
  return userElement;
}

export const renderRoom = (room) => {
  roomsListWrapperElement.style.display = 'none';
  roomWrapperElement.style.display = 'flex';

  // Get user which logged in
  currentUser = room.users.filter(user => user.username === gloabalUsername)[0];
  if (!currentUser) {
    return;
  }

  const roomnameElement = document.getElementById('room-roomname');
  roomnameElement.innerHTML = `<h1>${room.name}</h1>`;

  // Back to rooms list
  roomBackButton.addEventListener('click', () => {
    window.location.replace("http://localhost:3002"); 
  });
  
  // Toggle READY btn
  if (!currentUser.isReady) {
    roomReadyButton.innerHTML = 'READY';
  } else {
    roomReadyButton.innerHTML = 'NOT READY';
  }

  // Get users HTML elements
  const allUsers = room.users.map(user => getUserElement(user));

  const usersContainer = document.getElementById('room-users-container');
  usersContainer.innerHTML = "";
  usersContainer.append(...allUsers);

  const { state, ...value } = room.game;
  if (state) {
    switch(state) {
      case 'START_TIMER': return renderStartTimer(value.startTimerSecondsLeft);
      case 'GAME': return startGame(value);
      case 'GAME_FINISHED': return renderResults(value);
    }
  }
}

export const renderStartTimer = newTimerValue => {
  roomBackButton.style.display = 'none';
  roomReadyButton.style.display = 'none';
  roomStartTimer.innerHTML = newTimerValue;
}

function keyboardEventListener (event) {
  emitKeyboardEvent(event.key);
}

// Get text from cache (local variable) or fetch
const getText = (textId) => {
  return new Promise(resolve => {
    if (text !== '') 
      resolve(text);
    fetch(`http://localhost:3002/game/texts/${textId}`, {
      method: 'GET'
    })
    .then(data => data.json())
    .then(data => {
      resolve(data.text);
    });
  })
}

// Render game field
const startGame = (gameInfo) => {
  const { textId, gameSecondsLeft: secondsLeft } = gameInfo;
  getText(textId)
  .then(text => {
    roomStartTimer.style.display = 'none';
    // Highlight progress bar
    const userProgress = gameInfo.users.find(({ userInfo }) => userInfo.username === gloabalUsername).progress;
    roomText.innerHTML = text;
    roomText.innerHTML = highlight(roomText, userProgress).innerHTML;
    //
    roomSecondsLeft.innerHTML = `${secondsLeft} seconds left`;

    for (const user of gameInfo.users) {
      const { username } = user.userInfo;
      const progress = Math.floor(user.progress * 100 / text.length); 
      const userProgressElement = document.getElementById(`${username}-user-progress`);
      userProgressElement.style.width = progress + '%';
      if (user.progress === text.length) {
        userProgressElement.style.backgroundColor = 'greenyellow';
      }
    }
  })
  
  if (!isGameStarted) {
    document.addEventListener('keypress', keyboardEventListener);
    isGameStarted = true;
  }
}

// Reset room DOM && event listeners
const finishGame = () => {
  document.removeEventListener('keypress', keyboardEventListener);
  roomStartTimer.style.display = 'none';
  roomSecondsLeft.innerHTML = '';
  roomText.innerHTML = '';
  roomBackButton.style.display = 'flex';
  roomReadyButton.style.display = 'flex';
  isGameStarted = false;
  updateUser({
    username: gloabalUsername,
    isReady: false
  })
}

// Render modal with results
const renderResults = (gameInfo) => {
  const { winners } = gameInfo;
  const winnersInfo = winners.map((winner, index) => {
    return `#${index + 1} ${winner.userInfo.username} \n`
  });

  const bodyElement = createElement({
    tagName: "div"
  });
  bodyElement.innerHTML = winnersInfo;

  function onClose() {
    finishGame();
  } 

  showModal({
    title: 'winners',
    bodyElement,
    onClose
  });
}