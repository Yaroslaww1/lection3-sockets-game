import { createElement } from './helper.mjs';

export function showModal(obj) {
  const { title, bodyElement, onClose } = obj;
  const root = getModalContainer();
  const modal = createModal({ title, bodyElement, onClose }); 
  
  root.append(modal);
}

function getModalContainer() {
  return document.getElementById('root');
}

function createModal(obj) {
  const { title, bodyElement, onClose } = obj;
  const layer = createElement({
    tagName: "div",
    className: 'modal-layer'
  });
  const modalContainer = createElement({
    tagName: "div",
    className: 'modal-root'
  });
  const header = createHeader({ title, onClose });

  modalContainer.append(header, bodyElement);
  layer.append(modalContainer);

  return layer;
}

function createHeader(obj) {
  const { title, onClose } = obj;
  const headerElement = createElement({
    tagName: "div",
    className: 'modal-header'
  });
  const titleElement = createElement({
    tagName: "span",
    className: ''
  });
  const closeButton = createElement({
    tagName: "div",
    className: 'close-btn'
  });
  
  titleElement.innerText = title;
  closeButton.innerText = '×';
  
  const close = () => {
    hideModal();
    onClose();
  }
  closeButton.addEventListener('click', close);
  headerElement.append(title, closeButton);
  
  return headerElement;
}

function hideModal() {
  const modal = document.getElementsByClassName('modal-layer')[0];
  modal?.remove();
}