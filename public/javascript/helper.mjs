export const createElement = ({ tagName, className, attributes = {} }) => {
  const element = document.createElement(tagName);

  if (className) {
    addClass(element, className);
  }

  Object.keys(attributes).forEach(key => element.setAttribute(key, attributes[key]));

  return element;
};

export const addClass = (element, className) => {
  const classNames = formatClassNames(className);
  element.classList.add(...classNames);
};

export const removeClass = (element, className) => {
  const classNames = formatClassNames(className);
  element.classList.remove(...classNames);
};

export const formatClassNames = className => className.split(" ").filter(Boolean);

export const highlight = (inputElement, symbolsNumber) => {
  var innerHTML = inputElement.innerHTML;
  if (symbolsNumber >= 0) { 
   innerHTML = 
    '<span class="highlight">' + 
    innerHTML.substring(0, symbolsNumber) + 
    '</span>' + 
    '<span class="underline">' +
    innerHTML.substring(symbolsNumber, symbolsNumber + 1) +
    '</span>' + 
    innerHTML.substring(symbolsNumber + 1);
    inputElement.innerHTML = innerHTML;
  }

  return inputElement;
}