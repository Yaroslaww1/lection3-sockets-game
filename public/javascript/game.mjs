import { 
  renderRoom,
  renderRoomsList
} from "./render.mjs";

const username = sessionStorage.getItem("username");

if (!username) {
  window.location.replace("/login");
}

const socket = io("http://localhost:3002", { query: { username } });

const createRoom = () => {
  const name = window.prompt('Set name for room');
  socket.emit('CREATE_ROOM', name);
}

export const emitUpdateUser = user => {socket.emit('UPDATE_USER', user);}
export const emitKeyboardEvent = key => {socket.emit('KEYBOARD_PRESS', username, key)}
const emitJoinRoom = name => {socket.emit('JOIN_ROOM', name)}

const joinRoomHandler = room => {
  renderRoom(room);
}

renderRoomsList(createRoom, emitJoinRoom, []);

const updateRoomsHandler = rooms => {
  renderRoomsList(createRoom, emitJoinRoom, rooms);
}

const updateRoomHandler = room => {
  renderRoom(room,);
}

const errorHandler = (errorType, errorMessage) => {
  function usernameExist() {
    window.alert(errorMessage);
    sessionStorage.removeItem("username");
    window.location.replace("/login");
  }

  function roomnameExist() {
    window.alert(errorMessage);
  }

  switch (errorType) {
    case 'USERNAME_EXISTS': return usernameExist();
    case 'ROOMNAME_EXISTS': return roomnameExist();
  }
}

socket.on('UPDATE_ROOMS', updateRoomsHandler);
socket.on('UPDATE_ROOM', updateRoomHandler);
socket.on('SEND_ERROR', errorHandler);
socket.on('JOIN_ROOM', joinRoomHandler);