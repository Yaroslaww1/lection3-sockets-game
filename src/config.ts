import path from "path";

export const STATIC_PATH = path.join(__dirname, "../public");
export const HTML_FILES_PATH = path.join(STATIC_PATH, "html");

export const MAXIMUM_USERS_FOR_ONE_ROOM = 4;

export const SECONDS_TIMER_BEFORE_START_GAME = 1;

export const SECONDS_FOR_GAME = 20;

export const PORT = 3002;