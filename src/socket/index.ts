import socketIO from 'socket.io';
import UserService from './services/UserService';
import RoomService from './services/RoomService';
import { IRoomState, Room } from './services/Room';
import { User } from './services/User';
import { Game, IGameState } from './services/Game';

const enum UserActions {
  CREATE_ROOM = 'CREATE_ROOM',
  JOIN_ROOM = 'JOIN_ROOM',
  UPDATE_USER = 'UPDATE_USER',
  KEYBOARD_PRESS = 'KEYBOARD_PRESS'
}

const enum GlobalActions {
  UPDATE_ROOMS = 'UPDATE_ROOMS',
  UPDATE_ROOM = 'UPDATE_ROOM',
  SEND_ERROR = 'SEND_ERROR'
}

const enum ErrorTypes {
  USERNAME_EXISTS = 'USERNAME_EXISTS',
  ROOMNAME_EXISTS = 'ROOMNAME_EXISTS'
}

export default (io: socketIO.Server): void => {
  const userService = new UserService();
  const roomService = new RoomService();

  io.on('connection', socket => {
    let user: User;
    try {
      user = new User(socket.handshake.query.username);
      userService.addUser(user);
    } catch (error) {
      socket.emit(GlobalActions.SEND_ERROR, ErrorTypes.USERNAME_EXISTS, error.message);
      return;
    }

    // 'lobby' - room for current-not-in-any-room users
    socket.join('lobby');
    // Show rooms for new user
    socket.emit(GlobalActions.UPDATE_ROOMS, roomService.getAllRooms());

    socket.on(UserActions.CREATE_ROOM, (name: string) => {
      // now game can emit to socket directly
      const emitGameAction = (gameState: IGameState) => 
        io.to(name).emit(GlobalActions.UPDATE_ROOM, {...room, game: gameState });
      let room: Room;
      try {
        const game = new Game(emitGameAction);
        room = new Room(game, name, []);

        room.addUser(user);
        socket.join(room.name);
        socket.leave('lobby');

        roomService.addRoom(room);
      } catch (error) {
        socket.emit(GlobalActions.SEND_ERROR, ErrorTypes.ROOMNAME_EXISTS, error.message);
        return;
      }

      socket.emit(UserActions.JOIN_ROOM, room);
      // Created new room - update rooms on client
      io.to('lobby').emit(GlobalActions.UPDATE_ROOMS, roomService.getAllRooms());
    })

    socket.on(UserActions.JOIN_ROOM, (roomname: string) => {
      const room = roomService.getRoomByRoomname(roomname);
      if (room) {
        room.addUser(user);
        socket.leave('lobby');
        socket.join(room.name);
      }
      // Render room on client
      socket.emit(UserActions.JOIN_ROOM, room);
      // New user in room - Update room for users in this room (if any)
      socket.to(roomname).emit(GlobalActions.UPDATE_ROOM, room);
      // Update user-counter for one room
      io.to('lobby').emit(GlobalActions.UPDATE_ROOMS, roomService.getAllRooms());
    })

    socket.on(UserActions.UPDATE_USER, (newUser: User) => {
      try {
        // Find room in which user is and update Room and User
        const room = roomService.getRoomByUsername(newUser.username) as Room;
        room.updateUserByUsername(newUser.username, newUser);
        roomService.updateRoomByRoomname(room.name, room);
        userService.updateUserbyUsername(newUser.username, newUser);
      } catch (error) {
        console.error(error);
        return;
      }

      const room = roomService.getRoomByUsername(user.username) as Room;
      // User toggle READY | NON_READY in room - Update room for users in this room (if any)
      io.to(room.name).emit(GlobalActions.UPDATE_ROOM, room);
      // Update user-counter for one room (this action triggers in READY | NON-READY toggle)
      io.to('lobby').emit(GlobalActions.UPDATE_ROOMS, roomService.getAllRooms());
    })

    socket.on(UserActions.KEYBOARD_PRESS, (username: string, keycode: string) => {
      const room = roomService.getRoomByUsername(username) as Room;
      room.game.handleKeypress(username, keycode);
      roomService.updateRoomByRoomname(room.name, room);
    })

    socket.on('disconnecting', () => {
      const { username } = socket.handshake.query;
      userService.removeUserByUsername(username);
      // Get roomname of socket (filter because Object.keys(socket.rooms) also return unique for every socket)
      const roomname =  Object.keys(socket.rooms).filter(room => room !== socket.id)[0];
      const room = roomService.getRoomByRoomname(roomname);
      // Dont sure that each of these 2 lines are needed :/ 
      room?.removeUserByUsername(username);
      roomService.removeUserByUsername(username);

      io.to(roomname).emit(GlobalActions.UPDATE_ROOM, room);
      socket.join('lobby');
      // Update room-list state for all users in lobby
      io.to('lobby').emit(GlobalActions.UPDATE_ROOMS, roomService.getAllRooms());
    })
  });
};