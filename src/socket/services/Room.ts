import { User } from "./User";
import { MAXIMUM_USERS_FOR_ONE_ROOM } from '../../config'; 
import { Game } from "./Game";

export interface IRoomState {
  game: Game,
  name: string,
  users: User[]
}

export class Room {
  public isAvailable = true;
  constructor(
    public game: Game,
    public name: string,
    public users: User[],
  ) {
    this.game.registerRoomDataGetter(() => this);
  }

  private startGame(): void {
    this.isAvailable = false;
    this.game.start()
      .then(() => {
        // Game finished, create new game
        this.game = new Game(this.game.emit);
        this.users = this.users.map(user => ({
          ...user,
          isReady: false
        }));
        // Make room available to join
        this.isAvailable = true;
        this.game.registerRoomDataGetter(() => this);
      })
  }

  private canGameBeStarted(): boolean {
    const notReadyUsers = this.users.filter(user => !user.isReady);
    return notReadyUsers.length === 0;
  }

  updateUserByUsername(username: string, newUser: User): void {
    this.users = this.users.map(user => {
      if (user.username === username)
        Object.assign(user, newUser);
      return user;
    });

    if (this.canGameBeStarted())
      this.startGame();
  }

  addUser(user: User): void {
    this.users.push(user);
    if (this.users.length === MAXIMUM_USERS_FOR_ONE_ROOM) {
      this.isAvailable = false;
    }

    if (this.canGameBeStarted()) 
      this.startGame();
  }

  removeUserByUsername(username: string): void {
    this.users = this.users.filter(user => {
      return user.username !== username;
    })
    if (this.users.length > 0)
      this.isAvailable = true;

    this.game.state.users = this.game.state.users.filter(
      user => user.userInfo.username !== username
    );

    if (this.canGameBeStarted())
      this.startGame();
  }
}