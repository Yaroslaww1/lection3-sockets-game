import { Room } from './Room';

class RoomService {
  private rooms: Room[] = [];

  addRoom(room: Room): Room | never {
    const { name } = room;
    for (const room of this.rooms) {
      if (room.name === name)
        throw new Error(`Room with name ${name} already exists`);
    }
    this.rooms.push(room);
    return room;
  }

  getAllRooms(): Room[] {
    return this.rooms.filter(room => room.isAvailable);
  }

  getRoomByRoomname(roomname: string): Room | undefined {
    for (const room of this.rooms) {
      if (room.name === roomname)
        return room; 
    }
  }

  // Return Room in which user with username is
  getRoomByUsername(username: string): Room | undefined {
    for (const room of this.rooms) {
      if (room.users.some(user => user.username === username))
        return room;
    }
  }

  updateRoomByRoomname(roomname: string, newRoom: Room) : void {
    this.rooms = this.rooms.map(room => {
      if (room.name === roomname)
        Object.assign(room, newRoom);
      return room;
    })
  }

  /*
    Delets rooms from pool => room will never be shown in future 
    (its not the same as isAvailable; room with isAvailable = false can be shown in future)
  */
  private validateExistance(room: Room): boolean {
    if (room.users.length === 0)
      return false;
    return true;
  }
 
  removeUserByUsername(username: string): void {
    this.rooms = this.rooms.map(room => {
      room.removeUserByUsername(username);
      return room;
    })

    this.rooms = this.rooms.filter(room => this.validateExistance(room));
  }
}

export default RoomService;