import { User } from './User';

class UserService {
  private users: User[] = [];

  addUser(user: User): void | never {
    const { username } = user;
    for (const user of this.users) {
      if (user.username === username)
        throw new Error(`User with username ${username} already exists`)
    }
    this.users.push(user);
  }

  removeUserByUsername(username: string): void {
    this.users = this.users.filter(user => {
      return user.username !== username;
    })
  }

  getUserByUsername(username: string): User | undefined {
    return this.users.find(user => user.username === username);
  }

  updateUserbyUsername(username: string, newUser: User) : void {
    this.users = this.users.map(user => {
      if (user.username === username)
        Object.assign(user, newUser);
      return user;
    })
  }
}

export default UserService;