/* eslint-disable @typescript-eslint/no-explicit-any */
import { 
  SECONDS_TIMER_BEFORE_START_GAME,
  SECONDS_FOR_GAME
} from "../../config";
import { texts } from '../../api/data/data';
import { Room } from "./Room";
import { User } from "./User";

export const enum GameStates {
  START_TIMER = 'START_TIMER',
  GAME = 'GAME',
  GAME_FINISHED = 'GAME_FINISHED'
}

interface IGameUser {
  userInfo: User,
  progress: number
}

export interface IGameState {
  state: GameStates
  users: IGameUser[]
  winners: IGameUser[]
  textId: number
  gameSecondsLeft: number
  startTimerSecondsLeft: number
}

export class Game {
  public state: IGameState;
  // Get room data
  private getRoomData?: () => Room;
  private usersProgressMap: Record<string, number> = {};
  // text which is shown to the users as string (not id)
  private text?: string;

  constructor(
    public emit: (newGameState: IGameState) => void
  ) {
    this.state = {
      state: GameStates.START_TIMER,
      users: [],
      winners: [],
      textId: -1,
      gameSecondsLeft: SECONDS_FOR_GAME,
      startTimerSecondsLeft: SECONDS_TIMER_BEFORE_START_GAME
    }
  }

  registerRoomDataGetter(getter: () => Room): void {
    this.getRoomData = getter;
  }

  // emit game state changes to room-members
  emitStateUpdate(): void {
    this.emit(this.state);
  }

  // Start pre-game timer; resolves only after timeInSeconds seconds
  private startTimer(timeInSeconds: number): Promise<void> {
    return new Promise(resolve => {
      let timeLeft = timeInSeconds;
      const setTime = () => {
        this.state.state = GameStates.START_TIMER;
        this.state.startTimerSecondsLeft = timeLeft;
        // Update timer on client
        this.emitStateUpdate();
        timeLeft--;
        if (timeLeft <= 0) {
          clearInterval(id);
          resolve();
        }
      }
      const id = setInterval(setTime, 1000);
    })
  }

  // Remove winners who are not memeber of room (left during the game)
  private filterWinners(): void {
    this.state.winners = this.state.winners.filter(winner => {
      if (this.state.users.findIndex(user => user.userInfo.username === winner.userInfo.username) > -1)
        return true;
      return false;
    });
  }

  // Start game; resolves when all users are winners or after timeInSeconds seconds
  private startGame(timeInSeconds: number): Promise<void> {
    const textId = Math.floor(Math.random() * texts.length);
    this.text = texts[textId];
    this.state.textId = textId;
    // Send text to client
    this.emitStateUpdate();

    return new Promise(resolve => {
      let timeLeft = timeInSeconds;
      const setTime = () => {
        this.state.state = GameStates.GAME;
        this.state.gameSecondsLeft = timeLeft;
        // Send game timer to client
        this.emitStateUpdate();
        timeLeft--;
        this.filterWinners();
        if (timeLeft <= 0 || this.state.winners.length === this.state.users.length) {
          clearInterval(id);
          resolve();
        }
      }
      const id = setInterval(setTime, 1000);
    })
  }

  start(): Promise<void> {
    return new Promise(resolve => {
      if (this.getRoomData) {
        // Get room data and store in state
        const room = this.getRoomData();
        const { game, ...roomData } = room;
        const roomDataUsers = roomData.users.map(user => ({
          userInfo: user,
          progress: this.usersProgressMap[user.username] || 0
        }));
        this.state.users = roomDataUsers;
        this.emitStateUpdate();
      }
      const timer = this.startTimer(SECONDS_TIMER_BEFORE_START_GAME);
      timer
        .then(() => {
          this.startGame(SECONDS_FOR_GAME)
          .then(() => {
            this.state.state = GameStates.GAME_FINISHED;
            this.filterWinners();
            this.emitStateUpdate();
            resolve();
          })
        })
    })
  }

  // handle keypress of any user in room
  handleKeypress(username: string, keycode: string): void {
    const currentUserIndex = this.usersProgressMap[username] || 0;
    const currentUserChar: string = this.text?.charAt(currentUserIndex) as string;
    if (keycode === currentUserChar) {
      this.usersProgressMap[username] = currentUserIndex + 1;
      // Update progress for one user
      this.state.users = this.state.users.map(user => {
        if (user.userInfo.username === username)
          user.progress = this.usersProgressMap[username];
        return user;
      })
      // If user complete user added to winners
      if (currentUserIndex + 1 >= (this.text as string).length) {
        this.state.winners.push({
          progress: currentUserIndex + 1,
          userInfo: {
            username,
            isReady: true
          }
        });
        this.filterWinners();
      }
      // Update users states on client
      this.emitStateUpdate();
    }
  }
}