import express from 'express';
import http from 'http';
import socketIO from 'socket.io';
import { PORT, STATIC_PATH } from './config';
import routes from './api/routes/index';
import socketHandler from "./socket/index";
import errorHandlerMiddleware from './api/middlewares/errorHandlerMiddleware';

const app = express() as express.Application;
const httpServer = new http.Server(app);

const io = socketIO(httpServer) as socketIO.Server;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static(STATIC_PATH));

routes(app);

app.get('*', function (req, res, next) {
  res.redirect('/login');
})

app.use(errorHandlerMiddleware);

socketHandler(io);

httpServer.listen( PORT, () => {
  console.log( `server started at http://localhost:${ PORT }` );
} );
